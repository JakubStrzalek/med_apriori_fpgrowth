package com.med.config;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(commandDescription = "Runs association rules discover with chosen algorithm")
class CommandAlgorithm {

    @Parameter(names = {"--support", "-s"},
            description = "Minimal support for association rule to be chosen")
    private double support = 0.1;

    @Parameter(names = {"--confidence", "-c",},
            description = "Minimal support for association rule to be chosen")
    private double confidence = 0.8;

    @Parameter(names = {"--input", "-i"},
            description = "Input file to process")
    private String inputFileName = "input.data";

    @Parameter(names = {"--output", "-u"},
            description = "Output file with printed rules")
    private String outputFileName;

    public double getSupport() {
        return support;
    }

    public double getConfidence() {
        return confidence;
    }

    public String getInputFileName() {
        return inputFileName;
    }

    public String getOutputFileName() {
        return outputFileName;
    }
}
