package com.med.config;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import java.util.ArrayList;
import java.util.List;

@Parameters(commandDescription = "Runs association rules discover with chosen algorithm")
class CommandDebug {
    @Parameter
    private List<String> parameters = new ArrayList<>();

    @Parameter(names = {"--debug", "-d"}, description = "Debug mode")
    private boolean debug = false;

    public boolean isDebug() {
        return debug;
    }
}
