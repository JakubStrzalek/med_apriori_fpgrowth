package com.med.config;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.MissingCommandException;
import com.med.Parameter;

import java.util.List;

/**
 * Created by servlok on 24.01.16.
 */
public class CommandConsole {
    private JCommander jCommander;
    private CommandDebug commandDebug;
    private CommandAlgorithm commandAlgorithm;

    public static CommandConsole createCommand() {
        CommandConsole console = new CommandConsole();
        console.commandDebug = new CommandDebug();
        console.commandAlgorithm = new CommandAlgorithm();
        console.jCommander = new JCommander(console.commandDebug);
        console.jCommander.addCommand("fpgrowth", console.commandAlgorithm);
        console.jCommander.addCommand("apriori", console.commandAlgorithm);
        return console;
    }

    public boolean parseArgs(String[] args) {
        try {
            jCommander.parse(args);
        } catch (MissingCommandException e) {
            System.err.println("Unknown command");
        } finally {
            if (jCommander.getParsedCommand() == null) {
                jCommander.usage();
                return true;
            }
            return false;
        }
    }

    public Parameter getParameter(List<List<String>> transactions) {
        return new Parameter(commandAlgorithm.getSupport(), commandAlgorithm.getConfidence(), transactions.size());
    }

    public String inputFileName() {
        return commandAlgorithm.getInputFileName();
    }

    public String outputFileName() {
        return commandAlgorithm.getOutputFileName();
    }

    public boolean isDebug() {
        return commandDebug.isDebug();
    }

    public String getAlgorithmNameToRun() {
        return jCommander.getParsedCommand();
    }




}
