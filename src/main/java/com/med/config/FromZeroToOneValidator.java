package com.med.config;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.ParameterException;

class FromZeroToOneValidator implements IParameterValidator {
    @Override
    public void validate(String name, String value) throws ParameterException {
        float argument = Float.parseFloat(value);
        if (argument < 0.0f || argument > 1.0f) {
            throw new ParameterException("Parameter " + name + " should be " +
                    "greater-equal-than 0 and less-equal-than 1");
        }
    }
}
