package com.med;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class DataLoader {

    private final static String SEPARATOR = ",";

    /**
     * Tutaj zastosowałem "doklejenie" do kazdej wartosci parametru probki
     * nazwe numeru parametru np.
     * zamiast wartosci 3 parametru "p"
     * otrzymamy w wyniku "3=p"
     */
    public List<List<String>> loadDataWithAddedArgumentLabel(String fileName) throws IOException {
        List<String> samples = Files.readAllLines(Paths.get(fileName));
        List<List<String>> proccessedSamples = new ArrayList<>();


        for (String sample : samples) {
            String[] arguments = sample.split(SEPARATOR);
            List<String> result = new ArrayList<>();
            for (int i = 0; i < arguments.length; ++i)
                result.add((1 + i) + "=" + arguments[i]);

            proccessedSamples.add(result);
        }

        return proccessedSamples;
    }

    public List<List<String>> loadData(String fileName) throws IOException {
        return Files.lines(Paths.get(fileName))
                .map(s -> Arrays.asList(s.split(SEPARATOR)))
                .collect(Collectors.toList());
    }
}

