package com.med.util;

import com.med.Application;

public class Timer {

    private long startTime;
    private long diffTime;

    public void start() {
        startTime = System.currentTimeMillis();
    }

    public void stop() {
        long endTime = System.currentTimeMillis();
        diffTime = endTime - startTime;
    }

    @Override
    public String toString() {
        return "Elapsed time in seconds: " + (diffTime / 1_000.0);
    }

    public void log(String str) {
        if (Application.debug) {
            System.out.println(str + "Elapsed time in seconds: " + (diffTime / 1_000.0));
        }
    }

    public void log() {
        log("");
    }

    public long getDiffTime() {
        return diffTime;
    }

}
