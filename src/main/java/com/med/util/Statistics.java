package com.med.util;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class Statistics {

    public static void print(List<List<String>> transactions) {
        System.out.println("Number of transactions: " + transactions.size());
        System.out.println("Number of all items: " + transactions.stream().flatMap(Collection::stream).count());
        System.out.println("Number of unique items: " + transactions.stream().flatMap(Collection::stream)
                .collect(Collectors.toSet()).size());
    }
}
