package com.med;

import com.med.algorithm.Rule;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by servlok on 24.01.16.
 */
public class DataSaver {

    public void save(String fileName, List<Rule> rules) throws IOException {
        List<String> lines = rules.stream()
                .map(Rule::toString)
                .collect(Collectors.toList());

        Files.write(Paths.get(fileName), lines);
    }
}
