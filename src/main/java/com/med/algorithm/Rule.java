package com.med.algorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class Rule {
    private final List<String> antecedent;
    private final List<String> consequent;

    public Rule(Set<String> antecedent, Set<String> consequent) {
        this.antecedent = new ArrayList<>(antecedent);
        this.consequent = new ArrayList<>(consequent);
        Collections.sort(this.antecedent);
        Collections.sort(this.consequent);
    }

    public List<String> getAntecedent() {
        return antecedent;
    }

    public List<String> getConsequent() {
        return consequent;
    }


    @Override
    public String toString() {
        return antecedent + " => " + consequent;
    }

}
