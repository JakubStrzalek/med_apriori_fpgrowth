package com.med.algorithm.apriori;

import com.med.Parameter;
import com.med.algorithm.Algorithm;
import com.med.algorithm.Rule;

import java.util.*;
import java.util.stream.Collectors;

public class Apriori implements Algorithm {

    private List<String> transactions;
    private Map<String, Character> itemDictionary;
    private Map<Character, String> rItemDictionary;
    private Parameter parameter;
    private List<Itemset> allFreqItemsets;
    private Set<AsRule> allStrongRules;

    public Apriori(List<List<String>> transactions, Parameter parameter) {
        convertInput(transactions);
        this.parameter = parameter;
    }

    public static String remaingItems(String subset, String itemset) {
        StringBuilder consequent = new StringBuilder(itemset);
        for (int i = 0; i < subset.length(); i++) {
            int index = consequent.indexOf(String.valueOf(subset.charAt(i)));
            consequent.deleteCharAt(index);
        }

        return consequent.toString();
    }

    private static List<String> generateInitConsequents(Itemset itemset) {
        int nItems = itemset.getItems().length();
        List<String> consequents = new ArrayList<>(nItems);

        for (int i = 0; i < nItems; ++i) {
            consequents.add(String.valueOf(itemset.getItems().charAt(i)));
        }

        return consequents;
    }

    public List<Itemset> getAllFreqItemsets() {
        return allFreqItemsets;
    }

    @Override
    public void calcFreqItemsets() {
        findFreqItemsets();
    }

    @Override
    public void run() {
        this.allFreqItemsets = findFreqItemsets();
        this.allStrongRules = findRules(this.allFreqItemsets);
    }

    @Override
    public List<Rule> getRules() {
        return convertResult();
    }

    public Set<AsRule> findRules(List<Itemset> allFreqItemsets) {
        Set<AsRule> allStrongRules = new HashSet<>();

        allFreqItemsets.stream()
                .filter(itemset -> itemset.getItems().length() > 0)
                .forEach(itemset -> {
                    List<String> consequents = generateInitConsequents(itemset);

                    List<String> toRemove = new ArrayList<>(consequents.size());
                    do {
                        for (String consequent : consequents) {
                            String antecedent = remaingItems(consequent, itemset.getItems());
                            double confidence = itemset.getSupport() / calcSupport(antecedent);
                            if (confidence > parameter.getMinConfidence()) {
                                allStrongRules.add(new AsRule(
                                        antecedent,
                                        consequent,
                                        itemset.getSupport(),
                                        confidence));
                            } else {
                                toRemove.add(consequent);
                            }
                        }
                        consequents.removeAll(toRemove);
                        consequents = generateConsequents(consequents);
                    } while (!consequents.isEmpty());
                });

        return allStrongRules;
    }

    public List<Itemset> findFreqItemsets() {
        List<Itemset> allFreqItemsets = new ArrayList<>();
        allFreqItemsets.add(new Itemset("", 1.0));  // add empty itemset
        List<String> freqItemsetsCandidates = findInitFreqItemsets(); // itemsets with one element
        List<Itemset> freqItemsets = verifyFreqItemsetsCandidates(freqItemsetsCandidates);
        allFreqItemsets.addAll(freqItemsets);

        do {
            freqItemsets = generateFreqItemsets(freqItemsets);
            allFreqItemsets.addAll(freqItemsets);
        } while (!freqItemsets.isEmpty());

        return allFreqItemsets;
    }

    private List<String> findInitFreqItemsets() {
        return itemDictionary.values().stream()
                .sorted()
                .map(ch -> {
                    char[] chArr = {ch};
                    return new String(chArr);
                }).collect(Collectors.toList());

    }

    private List<Itemset> generateFreqItemsets(List<Itemset> freqItemsets) {
        int nFreqItemsets = freqItemsets.size();
        List<Itemset> nextFreqItemsets = new ArrayList<>();

        for (int i=0; i < nFreqItemsets; ++i) {
            String first = freqItemsets.get(i).getItems();
            String fEnding = first.substring(1, first.length());
            for (int j=i+1; j < nFreqItemsets; ++j) {
                String second = freqItemsets.get(j).getItems();
                String sBegining = second.substring(0, second.length() - 1);
                if (fEnding.equals(sBegining)) {
                    String candidate = String.valueOf(
                            first.charAt(0))
                            + fEnding
                            + second.charAt(second.length() - 1);
                    Itemset itemset = verifyCandidate(candidate);
                    if (itemset != null) {
                        nextFreqItemsets.add(itemset);
                    }
                }
            }
        }

        return nextFreqItemsets;
    }

    private List<String> generateConsequents(List<String> consequents) {
        int nConsequents = consequents.size();
        List<String> nextConsequents = new ArrayList<>();

        for (int i=0; i < nConsequents; ++i) {
            String first = consequents.get(i);
            String fEnding = first.substring(1, first.length());
            for (int j=i+1; j < nConsequents; ++j) {
                String second = consequents.get(j);
                String sBegining = second.substring(0, second.length() - 1);
                if (fEnding.equals(sBegining)) {
                    String consequent = String.valueOf(
                            first.charAt(0))
                            + fEnding
                            + second.charAt(second.length() - 1);
                    nextConsequents.add(consequent);
                }
            }
        }

        return nextConsequents;
    }

    private List<Itemset> verifyFreqItemsetsCandidates(List<String> candidates) {
        List<Itemset> freqItemsets = new ArrayList<>();
        long nTransactions = transactions.size();

        for (String c: candidates) {
            double support = 0.0;
            for (String t : transactions) {
                int j = -1;
                boolean containsItemset = true;
                for (int i=0; i < c.length(); ++i) {
                    j = t.indexOf(c.charAt(i), j+1);
                    if (j == -1) {
                        containsItemset = false;
                        break;
                    }

                }
                if (containsItemset) {
                    ++support;
                }
            }
            support /= nTransactions;
            if (support > parameter.getMinSupport()) {
                freqItemsets.add(new Itemset(c, support));
            }
        }

        return freqItemsets;
    }

    private double calcSupport(String itemset) {
        long nTransactions = transactions.size();

        double support = 0.0;
        for (String t : transactions) {
            int j = -1;
            boolean containsItemset = true;
            for (int i=0; i < itemset.length(); ++i) {
                j = t.indexOf(itemset.charAt(i), j+1);
                if (j == -1) {
                    containsItemset = false;
                    break;
                }

            }
            if (containsItemset) {
                ++support;
            }
        }
        return support / nTransactions;

    }

    private Itemset verifyCandidate(String candidate) {
        long nTransactions = transactions.size();

        double support = 0.0;
        for (String t : transactions) {
            int j = -1;
            boolean containsItemset = true;
            for (int i=0; i < candidate.length(); ++i) {
                j = t.indexOf(candidate.charAt(i), j+1);
                if (j == -1) {
                    containsItemset = false;
                    break;
                }

            }
            if (containsItemset) {
                ++support;
            }
        }
        support /= nTransactions;

        return support > parameter.getMinSupport()
                ? new Itemset(candidate, support)
                : null;
    }


    public List<String> getTransactions() {
        return transactions;
    }

    public Parameter getParameter() {
        return parameter;
    }

    private void convertInput(List<List<String>> transactions) {
        char ch = 'a';
//        char ch = 0;
        this.transactions = new ArrayList<>();
        this.itemDictionary = new HashMap<>();
        this.rItemDictionary = new HashMap<>();

        for (List<String> t : transactions) {
            String str = "";
            for (String item : t) {
                if (!itemDictionary.containsKey(item)) {
                    itemDictionary.put(item, ch);
                    rItemDictionary.put(ch, item);
                    str += ch;
                    ++ch;
                } else {
                    str +=  itemDictionary.get(item);
                }
            }
            char[] charArray = str.toCharArray();
            Arrays.sort(charArray);
            this.transactions.add(new String(charArray));
        }
    }

    private List<Rule> convertResult() {
        List<Rule> rules = new ArrayList<>(allStrongRules.size());
        for (AsRule r : allStrongRules) {
            Set<String> antecedent = new HashSet<>();
            Set<String> consequent = new HashSet<>();

            for (int i=0; i < r.getAntecedent().length(); ++i) {
                antecedent.add(rItemDictionary.get(r.getAntecedent().charAt(i)));
            }

            for (int i=0; i < r.getConsequent().length(); ++i) {
                consequent.add(rItemDictionary.get(r.getConsequent().charAt(i)));
            }

            rules.add(new Rule(antecedent, consequent));
        }

        return rules;
    }

}
