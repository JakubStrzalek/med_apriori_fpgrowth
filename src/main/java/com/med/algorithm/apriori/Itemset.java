package com.med.algorithm.apriori;

public class Itemset {

    private String items;
    private double support;

    public Itemset(String items, double support) {
        this.items = items;
        this.support = support;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public double getSupport() {
        return support;
    }

    public void setSupport(double support) {
        this.support = support;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Itemset itemset = (Itemset) o;

        return items.equals(itemset.getItems());
    }

    @Override
    public String toString() {
        return String.format("{ %s } %.5f", items, support);
    }
}
