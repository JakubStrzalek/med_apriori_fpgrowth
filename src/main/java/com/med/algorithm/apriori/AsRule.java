package com.med.algorithm.apriori;

public class AsRule {

    private double support;
    private double confidence;
    private String antecedent;
    private String consequent;

    public AsRule() {
    }

    public AsRule(String antecedent, String consequent) {
        this.antecedent = antecedent;
        this.consequent = consequent;
    }

    public AsRule(String antecedent, String consequent, double support, double confidence) {
        this.support = support;
        this.confidence = confidence;
        this.antecedent = antecedent;
        this.consequent = consequent;
    }

    public double getSupport() {
        return support;
    }

    public void setSupport(double support) {
        this.support = support;
    }

    public double getConfidence() {
        return confidence;
    }

    public void setConfidence(double confidence) {
        this.confidence = confidence;
    }

    public String getAntecedent() {
        return antecedent;
    }

    public void setAntecedent(String antecedent) {
        this.antecedent = antecedent;
    }

    public String getConsequent() {
        return consequent;
    }

    public void setConsequent(String consequent) {
        this.consequent = consequent;
    }

    @Override
    public String toString() {
        return String.format("{ %s } => { %s } %.5f %.5f", antecedent, consequent, support, confidence);
    }
}
