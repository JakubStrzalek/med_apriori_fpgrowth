package com.med.algorithm;

import java.util.List;

public interface Algorithm {

    void calcFreqItemsets();

    void run();

    List<Rule> getRules();
}
