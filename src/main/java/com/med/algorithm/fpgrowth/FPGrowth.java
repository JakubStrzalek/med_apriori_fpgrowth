package com.med.algorithm.fpgrowth;

import com.med.Parameter;
import com.med.algorithm.Algorithm;
import com.med.algorithm.Rule;
import com.med.algorithm.fpgrowth.tree.FPTree;
import com.med.algorithm.fpgrowth.tree.header.FPHeaderElement;
import com.med.util.ListUtils;

import java.util.*;
import java.util.stream.Collectors;

public class FPGrowth implements Algorithm {
    private FPTree fpTree;

    private List<List<String>> transactions;
    private int transactionsSize;

    private DataCompressor dataCompressor;
    private AssociationRulesGenerator associationRulesGenerator;

    private Parameter parameter;

    private Set<AsRule> associationRules;

    public FPGrowth(List<List<String>> transactions, Parameter parameter) {
        this.transactions = transactions;
        this.parameter = parameter;
        this.transactionsSize = transactions.size();

        associationRulesGenerator = new AssociationRulesGenerator(transactions, parameter);
    }

    @Override
    public void calcFreqItemsets() {
        compressDataBase();
        List<Itemset> freqItems = exploreFPTree(fpTree, new ArrayList<>());
    }

    @Override
    public void run() {
        compressDataBase();
        List<Itemset> freqItems = exploreFPTree(fpTree, new ArrayList<>());
        this.associationRules = associationRulesGenerator.findRules(freqItems);
    }

    @Override
    public List<Rule> getRules() {
        return associationRules.stream()
                .map(this::mapToRule)
                .collect(Collectors.toList());
    }

    private Rule mapToRule(AsRule rule) {
        return new Rule(new HashSet<>(rule.getAntecedent()), new HashSet<>(rule.getConsequent()));
    }


    void compressDataBase() {
        dataCompressor = new DataCompressor(transactions, parameter);

        dataCompressor.findAllOneElementsSetsFrequent();
        dataCompressor.removeNonFrequentElements();
        dataCompressor.sortTransactionDescSupportElement();

        this.fpTree = new FPTree(transactions, dataCompressor.getFrequencyOneElements(), parameter);


    }

    List<Itemset> exploreFPTree(FPTree tree, List<String> alfa) {
        List<Itemset> frequentItems = new ArrayList<>();
        if (tree.containsOnlyOnePath()) {
            List<String> path = tree.takeOnePath();
            for (List<String> beta : ListUtils.powerList(path)) {
                int support = minSupportFrom(beta, tree);
                beta.addAll(alfa);
                frequentItems.add(new Itemset(beta, (double) support / transactionsSize));
            }
            return frequentItems;
        } else {
            Map<String, FPHeaderElement> header = tree.getFpHeader().getElements();
            for (String alfa_i : header.keySet()) {
                List<String> beta = new ArrayList<>();
                beta.add(alfa_i);
                beta.addAll(alfa);

                frequentItems.add(new Itemset(beta, (double) header.get(alfa_i).getFrequency() / transactionsSize));

                List<List<String>> conditional_beta_base = tree.createConditionalBase(alfa_i);
                FPTree tree_beta = tree.createConditionalTree(conditional_beta_base);

                if (!tree_beta.isEmpty()) frequentItems.addAll(exploreFPTree(tree_beta, beta));
            }
            return frequentItems;
        }
    }

    private int minSupportFrom(List<String> beta, FPTree fpTree) {
        return beta.stream()
                .map(e -> fpTree.getFpHeader().getElements().get(e))
                .mapToInt(FPHeaderElement::getFrequency)
                .min()
                .orElse(fpTree.getEmptySetSupport());
    }

}
