package com.med.algorithm.fpgrowth.tree;

import com.med.Parameter;
import com.med.algorithm.fpgrowth.FrequencyOneElementsOperator;
import com.med.algorithm.fpgrowth.tree.header.FPHeader;
import com.med.algorithm.fpgrowth.tree.header.FPHeaderElement;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.med.algorithm.fpgrowth.tree.FPTreeNode.createRoot;

public class FPTree {

    FrequencyOneElementsOperator frequencyOneElementsOperator = new FrequencyOneElementsOperator();
    private Parameter parameter;
    private FPHeader fpHeader;
    private FPTreeNode root = createRoot();
    private int emptySetSupport;


    public FPTree(List<List<String>> transactions, Map<String, Integer> frequency, Parameter parameter) {
        this.fpHeader = new FPHeader(frequency);
        this.parameter = parameter;
        this.emptySetSupport = transactions.size();

        for (List<String> transaction : transactions) {
            addNewPath(transaction);
        }
    }

    private void addNewPath(List<String> transaction) {
        FPTreeNode pathNode = root;
        FPTreeNode father = null;
        for (String label : transaction) {
            father = pathNode;
            pathNode = pathNode.nextNodeAndIncrementCounter(label, father, fpHeader);
        }
    }

    public FPTreeNode getRoot() {
        return root;
    }

    public FPHeader getFpHeader() {
        return fpHeader;
    }

    public boolean containsOnlyOnePath() {
        if (isEmpty())
            return false;

        for (FPTreeNode node = root; node.getSons().size() != 0; node = node.getSons().get(0))
            if (node.getSons().size() > 1)
                return false;

        return true;

    }

    public List<String> takeOnePath() {
        if (!containsOnlyOnePath())
            throw new IllegalStateException();
        List<String> path = new ArrayList<>();
        for (FPTreeNode node = root; node.getSons().size() == 1; node = node.getSons().get(0))
            path.add(node.getSons().get(0).getLabel());
        return path;
    }

    public List<List<String>> createConditionalBase(String beta) {
        Map<String, FPHeaderElement> header = fpHeader.getElements();
        List<List<String>> paths = new ArrayList<>();
        if (header.get(beta) == null)
            throw new IllegalArgumentException();
        for (FPTreeNode finalNode : header.get(beta).getNodes()) {
            List<String> path = new ArrayList<>();
            for (FPTreeNode node = finalNode; !node.getFather().equals(root); node = node.getFather()) {
                path.add(0, node.getFather().getLabel());
            }
            for (int i = 0; i < finalNode.getCounter(); ++i)
                paths.add(path);
        }
        return paths;
    }

    public FPTree createConditionalTree(List<List<String>> conditionalBase) {
        Map<String, Integer> frequencyOneElements = frequencyOneElementsOperator.findAll(conditionalBase);

        Set<String> elementsToRemove = frequencyOneElementsOperator.findElementsToRemoveFromTransaction(
                parameter.getAbsoluteMinSupport(),
                frequencyOneElements
        );

        elementsToRemove.stream().forEach(frequencyOneElements::remove);

        for (List<String> path : conditionalBase)
            path.removeAll(elementsToRemove);

        return new FPTree(conditionalBase, frequencyOneElements, parameter);
    }

    public boolean isEmpty() {
        return root.getSons().size() == 0;
    }

    public int getEmptySetSupport() {
        return emptySetSupport;
    }
}
