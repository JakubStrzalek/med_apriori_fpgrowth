package com.med.algorithm.fpgrowth.tree.header;

import com.med.algorithm.fpgrowth.tree.FPTreeNode;

import java.util.HashMap;
import java.util.Map;

import static com.med.algorithm.fpgrowth.tree.header.FPHeaderElement.createNewRaw;

public class FPHeader {
    private Map<String, FPHeaderElement> elements = new HashMap<>();

    public FPHeader(Map<String, Integer> oneElementSetsFrequency) {
        oneElementSetsFrequency.keySet().stream()
                .forEach(e -> {
                    elements.put(e, createNewRaw(oneElementSetsFrequency.get(e)));
                });
    }

    public void addNode(FPTreeNode node) {
        FPHeaderElement headerElement = elements.get(node.getLabel());
        if (headerElement == null) {
            throw new IllegalArgumentException();
        }
        headerElement.addNode(node);

    }

    public Map<String, FPHeaderElement> getElements() {
        return elements;
    }

}
