package com.med.algorithm.fpgrowth.tree.header;

import com.med.algorithm.fpgrowth.tree.FPTreeNode;

import java.util.ArrayList;
import java.util.List;

public class FPHeaderElement {
    private final int frequency;
    private final List<FPTreeNode> nodes;

    private FPHeaderElement(int frequency, List<FPTreeNode> nodes) {
        this.frequency = frequency;
        this.nodes = nodes;
    }

    public static FPHeaderElement createNewRaw(int frequency) {
        return new FPHeaderElement(frequency, new ArrayList<>());
    }

    public void addNode(FPTreeNode node) {
        nodes.add(node);
    }

    public int getFrequency() {
        return frequency;
    }

    public List<FPTreeNode> getNodes() {
        return nodes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FPHeaderElement that = (FPHeaderElement) o;

        if (frequency != that.frequency) return false;
        return nodes.equals(that.nodes);

    }

    @Override
    public int hashCode() {
        int result = frequency;
        result = 31 * result + nodes.hashCode();
        return result;
    }
}
