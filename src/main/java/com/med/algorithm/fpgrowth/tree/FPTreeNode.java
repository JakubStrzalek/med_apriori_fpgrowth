package com.med.algorithm.fpgrowth.tree;

import com.med.algorithm.fpgrowth.tree.header.FPHeader;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class FPTreeNode {
    private final static String ROOT_LABEL = "null";
    private final FPTreeNode father;
    private final List<FPTreeNode> sons;
    private String label;
    private int counter;

    private FPTreeNode(String label, int counter, List<FPTreeNode> fpTreeNodes) {
        this(label, counter, fpTreeNodes, null);
    }

    private FPTreeNode(String label, int counter, List<FPTreeNode> fpTreeNodes, FPTreeNode father) {
        this.label = label;
        this.counter = counter;
        this.sons = fpTreeNodes;
        this.father = father;
    }

    public static FPTreeNode createRoot() {
        return new FPTreeNode(ROOT_LABEL, -1, new ArrayList<>());
    }

    private FPTreeNode createNode(String label, FPTreeNode father) {
        return new FPTreeNode(label, 0, new ArrayList<>(), father);
    }

    public int getCounter() {
        return counter;
    }

    public String getLabel() {
        return label;
    }

    public FPTreeNode nextNodeAndIncrementCounter(String nextLabel, FPTreeNode father, FPHeader header) {
        FPTreeNode nextNode = sons.stream()
                .filter(fpNode -> nextLabel.equals(fpNode.getLabel()))
                .findAny()
                .orElseGet(() -> {
                    FPTreeNode newNode = createNode(nextLabel, father);
                    this.sons.add(newNode);
                    header.addNode(newNode);
                    return newNode;
                });
        nextNode.counter++;

        return nextNode;
    }

    public Optional<FPTreeNode> getSon(String label) {
        return sons.stream()
                .filter(fpNode -> label.equals(fpNode.getLabel()))
                .findAny();

    }

    public List<FPTreeNode> getSons() {
        return sons;
    }

    public FPTreeNode getFather() {
        return father;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FPTreeNode that = (FPTreeNode) o;

        if (counter != that.counter) return false;
        return label.equals(that.label);

    }

    @Override
    public int hashCode() {
        int result = label.hashCode();
        result = 31 * result + counter;
        return result;
    }

}
