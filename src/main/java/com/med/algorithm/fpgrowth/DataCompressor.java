package com.med.algorithm.fpgrowth;

import com.med.Parameter;

import java.util.*;

class DataCompressor {

    private Map<String, Integer> frequencyOneElements = new HashMap<>();
    private Set<String> elementsToRemove = new HashSet<>();
    private List<List<String>> transactions;
    private Parameter parameter;

    private FrequencyOneElementsOperator frequencyOneElementsOperator = new FrequencyOneElementsOperator();

    DataCompressor(List<List<String>> transactions, Parameter parameter) {
        this.transactions = transactions;
        this.parameter = parameter;
    }

    void findAllOneElementsSetsFrequent() {
        frequencyOneElements = frequencyOneElementsOperator.findAll(transactions);

        elementsToRemove = frequencyOneElementsOperator.findElementsToRemoveFromTransaction(
                parameter.getAbsoluteMinSupport(),
                frequencyOneElements
        );

        elementsToRemove.stream().forEach(frequencyOneElements::remove);
    }

    void removeNonFrequentElements() {
        for (List<String> transaction : transactions)
            transaction.removeAll(elementsToRemove);
    }

    void sortTransactionDescSupportElement() {
        for (List<String> transaction : transactions)
            Collections.sort(transaction, (s1, s2) -> frequencyOneElements.get(s2) - frequencyOneElements.get(s1));
    }

    Map<String, Integer> deepCopyOfFrequencyOneElements() {
        Map<String, Integer> newFrequencyOneElements = new HashMap<>();
        for (String key : frequencyOneElements.keySet()) {
            newFrequencyOneElements.put(key, frequencyOneElements.get(key));
        }
        return newFrequencyOneElements;
    }

    Set<String> deepCopyOfElementsToRemove() {
        Set<String> newElementsToRemove = new HashSet<>();
        for (String element : elementsToRemove) {
            newElementsToRemove.add(element);
        }
        return newElementsToRemove;
    }

    public Map<String, Integer> getFrequencyOneElements() {
        return frequencyOneElements;
    }
}
