package com.med.algorithm.fpgrowth;

import com.google.common.collect.Lists;
import com.med.Parameter;
import com.med.util.ListUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

class AssociationRulesGenerator {

    private final Parameter parameter;
    private List<List<String>> transactions;

    public AssociationRulesGenerator(List<List<String>> transactions, Parameter parameter) {
        this.parameter = parameter;
        this.transactions = transactions;
    }

    public Set<AsRule> findRules(List<Itemset> allFreqItemsets) {
        Set<AsRule> allStrongRules = new HashSet<>();

        allFreqItemsets.stream()
                .filter(itemset -> itemset.getItems().size() > 0)
                .forEach(itemset -> {
                    List<List<String>> consequents = generateInitConsequents(itemset);

                    List<List<String>> toRemove = new ArrayList<>(consequents.size());
                    do {
                        for (List<String> consequent : consequents) {
                            List<String> antecedent = remaingItems(consequent, itemset.getItems());
                            double confidence = itemset.getSupport() / calcSupport(antecedent);
                            if (confidence > parameter.getMinConfidence()) {
                                allStrongRules.add(new AsRule(
                                        antecedent,
                                        consequent,
                                        itemset.getSupport(),
                                        confidence));
                            } else {
                                toRemove.add(consequent);
                            }
                        }
                        consequents.removeAll(toRemove);
                        consequents = generateConsequents(consequents);
                    } while (!consequents.isEmpty());
                });

        return allStrongRules;
    }

    private List<String> remaingItems(List<String> consequent, List<String> items) {
        return ListUtils.complementaryList(items, consequent);
    }

    private List<List<String>> generateConsequents(List<List<String>> consequents) {
        int nConsequents = consequents.size();
        List<List<String>> nextConsequents = new ArrayList<>();

        for (int i = 0; i < nConsequents; ++i) {
            List<String> first = consequents.get(i);
            List<String> fEnding = first.stream()
                    .filter(e -> !e.equals(first.get(0)))
                    .collect(Collectors.toList());
            for (int j = i + 1; j < nConsequents; ++j) {
                List<String> second = consequents.get(j);
                List<String> sBegining = second.stream()
                        .filter(e -> !e.equals(second.get(second.size() - 1)))
                        .collect(Collectors.toList());
                if (fEnding.equals(sBegining)) {
                    List<String> consequent = new ArrayList<>();
                    consequent.add(first.get(0));
                    consequent.addAll(fEnding);
                    consequent.add(second.get(second.size() - 1));
                    nextConsequents.add(consequent);
                }
            }
        }

        return nextConsequents;
    }

    private List<List<String>> generateInitConsequents(Itemset itemset) {
        int nItems = itemset.getItems().size();
        List<List<String>> consequents = new ArrayList<>(nItems);

        for (String element : itemset.getItems()) {
            consequents.add(Lists.newArrayList(element));
        }

        return consequents;
    }

    private double calcSupport(List<String> itemSet) {
        long nTransactions = transactions.size();

        boolean isOk = true;
        double support = 0.0;
        for (List<String> t : transactions) {
            isOk = true;
            for (String element : itemSet) {
                if (!t.contains(element)) {
                    isOk = false;
                    break;
                }
            }
            if (isOk) ++support;
        }
        return support / nTransactions;
    }


}
