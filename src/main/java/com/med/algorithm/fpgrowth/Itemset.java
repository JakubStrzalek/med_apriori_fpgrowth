package com.med.algorithm.fpgrowth;

import java.util.List;

class Itemset {

    private List<String> items;
    private double support;

    public Itemset(List<String> items, double support) {
        this.items = items;
        this.support = support;
    }

    public List<String> getItems() {
        return items;
    }

    public void setItems(List<String> items) {
        this.items = items;
    }

    public double getSupport() {
        return support;
    }

    public void setSupport(double support) {
        this.support = support;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Itemset itemset = (Itemset) o;

        return items.equals(itemset.getItems());
    }

    @Override
    public String toString() {
        return String.format("{ %s } %.5f", items, support);
    }
}
