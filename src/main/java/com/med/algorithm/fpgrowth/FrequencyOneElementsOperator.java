package com.med.algorithm.fpgrowth;

import java.util.*;

public class FrequencyOneElementsOperator {
    public Map<String, Integer> findAll(List<List<String>> transactions) {
        Map<String, Integer> frequencyOneElements = new HashMap<>();
        transactions.stream()
                .flatMap(Collection::stream)
                .forEach(s -> {
                    Integer count = frequencyOneElements.get(s);
                    if (count == null)
                        count = 0;

                    frequencyOneElements.put(s, ++count);
                });
        return frequencyOneElements;
    }

    public Set<String> findElementsToRemoveFromTransaction(double minSupport, Map<String, Integer> frequencyOneElements) {
        Set<String> elementsToRemove = new HashSet<>();
        frequencyOneElements.keySet().stream()
                .filter(element -> frequencyOneElements.get(element) < minSupport)
                .forEach(elementsToRemove::add);

        return elementsToRemove;
    }


}
