package com.med.algorithm.fpgrowth;

import java.util.List;

class AsRule {

    private double support;
    private double confidence;
    private List<String> antecedent;
    private List<String> consequent;

    public AsRule() {
    }

    public AsRule(List<String> antecedent, List<String> consequent, double support, double confidence) {
        this.support = support;
        this.confidence = confidence;
        this.antecedent = antecedent;

        this.consequent = consequent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AsRule asRule = (AsRule) o;

        if (Double.compare(asRule.support, support) != 0) return false;
        if (Double.compare(asRule.confidence, confidence) != 0) return false;
        if (antecedent != null ? !antecedent.equals(asRule.antecedent) : asRule.antecedent != null) return false;
        return consequent != null ? consequent.equals(asRule.consequent) : asRule.consequent == null;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(support);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(confidence);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (antecedent != null ? antecedent.hashCode() : 0);
        result = 31 * result + (consequent != null ? consequent.hashCode() : 0);
        return result;
    }

    public double getSupport() {
        return support;
    }

    public void setSupport(double support) {
        this.support = support;
    }

    public double getConfidence() {
        return confidence;
    }

    public void setConfidence(double confidence) {
        this.confidence = confidence;
    }

    public List<String> getAntecedent() {
        return antecedent;
    }

    public void setAntecedent(List<String> antecedent) {
        this.antecedent = antecedent;
    }

    public List<String> getConsequent() {
        return consequent;
    }

    public void setConsequent(List<String> consequent) {
        this.consequent = consequent;
    }

    @Override
    public String toString() {
        return String.format("{ %s } => { %s } %.5f %.5f", antecedent, consequent, support, confidence);
    }
}
