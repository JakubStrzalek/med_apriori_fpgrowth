package com.med;

import com.med.algorithm.Algorithm;
import com.med.algorithm.Rule;
import com.med.algorithm.apriori.Apriori;
import com.med.algorithm.fpgrowth.FPGrowth;
import com.med.config.CommandConsole;
import com.med.util.Statistics;
import com.med.util.Timer;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Application {

    public static boolean debug = false;


    public static void main(String[] args) {
        CommandConsole commandConsole = CommandConsole.createCommand();

        if (commandConsole.parseArgs(args)) {
            return;
        }

        DataLoader dl = new DataLoader();
        DataSaver ds = new DataSaver();


        String fileNameToLoadData = commandConsole.inputFileName();

        List<List<String>> data;

        System.out.println("Load file: " + commandConsole.inputFileName());
        System.out.println("Save file: " + commandConsole.outputFileName());


        try {
            data = dl.loadDataWithAddedArgumentLabel(fileNameToLoadData);
        } catch (IOException e) {
            System.err.println("Incorrect loaded file name!");
            return;
        }

        debug = commandConsole.isDebug();
        Parameter parameter = commandConsole.getParameter(data);

        Statistics.print(data);
        System.out.println("Support: " + parameter.getMinSupport());
        System.out.println("Confidence: " + parameter.getMinConfidence());

        Algorithm algorithm;
        switch (commandConsole.getAlgorithmNameToRun()) {
            case "apriori":
                algorithm = new Apriori(data, parameter);
                break;
            case "fpgrowth":
                algorithm = new FPGrowth(data, parameter);
                break;
            default:
                throw new RuntimeException("Incorrect algorithm name!");
        }


        Timer timer = new Timer();
        timer.start();

        algorithm.run();

        timer.stop();
        timer.log();

        List<Rule> rules = algorithm.getRules();

        rules.sort((rule1, rule2) -> rule1.toString().compareTo(rule2.toString()));

        String fileNameToSaveData = commandConsole.outputFileName();

        if (fileNameToSaveData == null) {
            rules.forEach(System.out::println);
        } else {
            try {
                ds.save(fileNameToSaveData, rules);
            } catch (IOException e) {
                System.err.println("Incorrect file name to save data!");
            }
        }

    }


}
