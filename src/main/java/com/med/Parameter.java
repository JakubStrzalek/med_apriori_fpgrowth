package com.med;

public class Parameter {
    private final double minSupport;
    private final double minConfidence;
    private final double absoluteMinSupport;

    public Parameter() {
        this(0, 0, 0);
    }

    public Parameter(double minSupport, double minConfidence) {
        this(minSupport, minConfidence, 0);
    }

    public Parameter(double minSupport, double minConfidence, int sizeTransaction) {
        this.minSupport = minSupport;
        this.minConfidence = minConfidence;
        this.absoluteMinSupport = minSupport * sizeTransaction;
    }

    public double getMinSupport() {
        return minSupport;
    }

    public double getMinConfidence() {
        return minConfidence;
    }

    public double getAbsoluteMinSupport() {
        return absoluteMinSupport;
    }
}
