package com.med.algorithm.apriori;

import com.med.DataLoader;
import com.med.Parameter;
import com.med.algorithm.Rule;
import org.junit.Test;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class AprioriTest {

    @Test
    public void test1() throws IOException {

//        apriori.setTransactions(dataLoader.loadDataWithAddedArgumentLabel("iris.data"));

        List<List<String>> transactions = new ArrayList<>();
        transactions.add(new ArrayList<>(Arrays.asList("a", "b", "c", "e")));
        transactions.add(new ArrayList<>(Arrays.asList("a", "b", "c", "e", "f")));
        transactions.add(new ArrayList<>(Arrays.asList("a", "b", "c", "h")));
        transactions.add(new ArrayList<>(Arrays.asList("a", "b", "e")));
        transactions.add(new ArrayList<>(Arrays.asList("a", "c", "f", "h")));
        transactions.add(new ArrayList<>(Arrays.asList("b", "e", "f")));
        transactions.add(new ArrayList<>(Arrays.asList("h")));
        transactions.add(new ArrayList<>(Arrays.asList("a", "f")));

        Apriori apriori = new Apriori(transactions, new Parameter(0.125, 0.6));

        List<String> transactions1 = apriori.getTransactions();
        transactions1.forEach(System.out::println);

        List<Itemset> freqItemsets = apriori.findFreqItemsets();
        Set<AsRule> strongRules = apriori.findRules(freqItemsets);
        freqItemsets.forEach(System.out::println);
        System.out.println(String.format("number of frequent itemsets: %d", freqItemsets.size()));
        strongRules.forEach(System.out::println);
        System.out.println(String.format("number of strong rules: %d", strongRules.size()));

        System.out.println("Done.");
    }

    @Test
    public void test2() throws IOException {

//        apriori.setTransactions(dataLoader.loadDataWithAddedArgumentLabel("iris.data"));

        List<List<String>> transactions = new ArrayList<>();
        transactions.add(new ArrayList<>(Arrays.asList("a", "b", "c", "e")));
        transactions.add(new ArrayList<>(Arrays.asList("a", "b", "c", "e", "f")));
        transactions.add(new ArrayList<>(Arrays.asList("a", "b", "c", "h")));
        transactions.add(new ArrayList<>(Arrays.asList("a", "b", "e")));
        transactions.add(new ArrayList<>(Arrays.asList("a", "c", "f", "h")));
        transactions.add(new ArrayList<>(Arrays.asList("b", "e", "f")));
        transactions.add(new ArrayList<>(Arrays.asList("h")));
        transactions.add(new ArrayList<>(Arrays.asList("a", "f")));

        Apriori apriori = new Apriori(transactions, new Parameter(0.125, 0.6));

        apriori.run();
        List<Rule> rules = apriori.getRules();
        rules.forEach(System.out::println);

        System.out.println("Done.");
    }

    @Test
    public void temp() {
        List<List<String>> transactions = new ArrayList<>();
        transactions.add(new ArrayList<>(Arrays.asList("a", "b", "c", "e")));
        transactions.add(new ArrayList<>(Arrays.asList("a", "b", "c", "e", "f")));
        transactions.add(new ArrayList<>(Arrays.asList("a", "b", "c", "h")));
        transactions.add(new ArrayList<>(Arrays.asList("a", "b", "e")));
        transactions.add(new ArrayList<>(Arrays.asList("a", "c", "f", "h")));
        transactions.add(new ArrayList<>(Arrays.asList("b", "e", "f")));
        transactions.add(new ArrayList<>(Arrays.asList("h")));
        transactions.add(new ArrayList<>(Arrays.asList("a", "f")));

        System.out.println("Number of unique items: " + transactions.stream().flatMap(Collection::stream)
                .collect(Collectors.toSet()).size());

        System.out.println("Unique items: " + transactions.stream().flatMap(Collection::stream)
                .collect(Collectors.toSet()));


    }
}
