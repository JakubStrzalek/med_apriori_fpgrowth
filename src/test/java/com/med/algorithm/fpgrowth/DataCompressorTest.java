package com.med.algorithm.fpgrowth;

import com.med.Parameter;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class DataCompressorTest {

    //Testing findAllOneElementsSetsFrequent
    @Test
    public void shouldReturnValidFrequent() {
        List<List<String>> transactions =
                Arrays.asList(
                        Arrays.asList("1a", "2a", "3a"),
                        Arrays.asList("1a", "2a", "3b"),
                        Arrays.asList("1a", "2b", "3c")
                );
        DataCompressor dataCompressor = new DataCompressor(transactions, new Parameter());
        dataCompressor.findAllOneElementsSetsFrequent();

        Map<String, Integer> frequency = dataCompressor.deepCopyOfFrequencyOneElements();

        assertEquals(3, (int) frequency.get("1a"));
        assertEquals(2, (int) frequency.get("2a"));
        assertEquals(1, (int) frequency.get("3a"));
        assertEquals(null, frequency.get("4a"));


    }

    @Test
    public void shouldOnlyOneFrequent() {
        List<List<String>> transactions =
                Arrays.asList(
                        Arrays.asList("1a", "2a", "3a"),
                        Arrays.asList("1a", "2b", "3b"),
                        Arrays.asList("1a", "2c", "3c")
                );
        DataCompressor dataCompressor = new DataCompressor(transactions, new Parameter(0.8, 0, transactions.size()));
        dataCompressor.findAllOneElementsSetsFrequent();

        Map<String, Integer> frequency = dataCompressor.deepCopyOfFrequencyOneElements();

        assertEquals(3, (int) frequency.get("1a"));
        assertEquals(null, frequency.get("2a"));
        assertEquals(null, frequency.get("3a"));
        assertEquals(null, frequency.get("4a"));


    }

    @Test
    public void shouldNoneBeFrequent() {
        List<List<String>> transactions =
                Arrays.asList(
                        Arrays.asList("1a", "2a", "3a"),
                        Arrays.asList("1b", "2b", "3b"),
                        Arrays.asList("1c", "2c", "3c")
                );
        DataCompressor dataCompressor = new DataCompressor(transactions, new Parameter(0.8, 0, transactions.size()));
        dataCompressor.findAllOneElementsSetsFrequent();

        Map<String, Integer> frequency = dataCompressor.deepCopyOfFrequencyOneElements();

        assertEquals(null, frequency.get("1a"));
        assertEquals(null, frequency.get("2a"));
        assertEquals(null, frequency.get("3a"));
        assertEquals(null, frequency.get("4a"));


    }

    //Testing removeNonFrequentElements
    @Test
    public void shouldAllBeFine() {
        List<List<String>> transactions =
                Arrays.asList(
                        Arrays.asList("1a", "2a", "3a"),
                        Arrays.asList("1b", "2b", "3b"),
                        Arrays.asList("1c", "2c", "3c")
                );
        DataCompressor dataCompressor = new DataCompressor(transactions, new Parameter(0.3, 0, transactions.size()));
        dataCompressor.findAllOneElementsSetsFrequent();
        dataCompressor.removeNonFrequentElements();

        assertEquals(
                Arrays.asList(
                        Arrays.asList("1a", "2a", "3a"),
                        Arrays.asList("1b", "2b", "3b"),
                        Arrays.asList("1c", "2c", "3c")
                ),
                transactions
        );
    }

    @Test
    public void shouldAllBeWhipeOut() {

        List<List<String>> transactions = new ArrayList<>();
        transactions.add(new ArrayList<>(Arrays.asList("1a", "2a", "3a")));
        transactions.add(new ArrayList<>(Arrays.asList("1b", "2b", "3b")));
        transactions.add(new ArrayList<>(Arrays.asList("1c", "2c", "3c")));

        DataCompressor dataCompressor = new DataCompressor(transactions, new Parameter(0.5, 0, transactions.size()));
        dataCompressor.findAllOneElementsSetsFrequent();
        dataCompressor.removeNonFrequentElements();

        assertEquals(
                Arrays.asList(
                        new ArrayList<>(),
                        new ArrayList<>(),
                        new ArrayList<>()
                ),
                transactions
        );
    }

    @Test
    public void shouldLeaveOnlyOneElementInFirstAndSecondTransaction() {

        List<List<String>> transactions = new ArrayList<>();
        transactions.add(new ArrayList<>(Arrays.asList("1a", "2a", "3a")));
        transactions.add(new ArrayList<>(Arrays.asList("1a", "2b", "3b")));
        transactions.add(new ArrayList<>(Arrays.asList("1c", "2c", "3c")));

        DataCompressor dataCompressor = new DataCompressor(transactions, new Parameter(0.5, 0, transactions.size()));
        dataCompressor.findAllOneElementsSetsFrequent();
        dataCompressor.removeNonFrequentElements();

        assertEquals(
                Arrays.asList(
                        new ArrayList<>(Arrays.asList("1a")),
                        new ArrayList<>(Arrays.asList("1a")),
                        new ArrayList<>()
                ),
                transactions
        );
    }

    @Test
    public void shouldBeValidTransactions() {

        List<List<String>> transactions = new ArrayList<>();
        transactions.add(new ArrayList<>(Arrays.asList("1a", "2a", "3a")));
        transactions.add(new ArrayList<>(Arrays.asList("1a", "2b", "3b")));
        transactions.add(new ArrayList<>(Arrays.asList("1c", "2a", "3c")));

        DataCompressor dataCompressor = new DataCompressor(transactions, new Parameter(0.5, 0, transactions.size()));
        dataCompressor.findAllOneElementsSetsFrequent();
        dataCompressor.removeNonFrequentElements();

        assertEquals(
                Arrays.asList(
                        new ArrayList<>(Arrays.asList("1a", "2a")),
                        new ArrayList<>(Arrays.asList("1a")),
                        new ArrayList<>(Arrays.asList("2a"))
                ),
                transactions
        );
    }

    //Test sortTransactionDescSupportElement

    @Test
    public void shouldBeValidDescSortWithTransaction() {

        List<List<String>> transactions = new ArrayList<>();
        transactions.add(new ArrayList<>(Arrays.asList("1a", "2a", "3a")));
        transactions.add(new ArrayList<>(Arrays.asList("1a", "2a", "3b")));
        transactions.add(new ArrayList<>(Arrays.asList("1c", "2a", "3a")));

        DataCompressor dataCompressor = new DataCompressor(transactions, new Parameter(0.5, 0, transactions.size()));
        dataCompressor.findAllOneElementsSetsFrequent();
        dataCompressor.removeNonFrequentElements();
        dataCompressor.sortTransactionDescSupportElement();

        assertEquals(
                Arrays.asList(
                        new ArrayList<>(Arrays.asList("2a", "1a", "3a")),
                        new ArrayList<>(Arrays.asList("2a", "1a")),
                        new ArrayList<>(Arrays.asList("2a", "3a"))
                ),
                transactions
        );
    }
}
