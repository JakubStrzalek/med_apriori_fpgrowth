package com.med.algorithm.fpgrowth;

import com.med.DataLoader;
import com.med.Parameter;
import com.med.algorithm.apriori.Apriori;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

public class FPGrowthMushroomTest {

    /**
     * plik iris.data powinien byc w projekcie
     */
    @Test
    @Ignore
    public void mushroomsDataTest() {
        DataLoader dl = new DataLoader();
        List<List<String>> transactions;
        try {
            transactions =
                    dl.loadDataWithAddedArgumentLabel("mushrooms.data");
        } catch (IOException e) {
            System.err.println("Błędna nazwa pliku!");
            return;
        }

        Parameter parameter = new Parameter(0.9, 0.98, transactions.size());

        Apriori apriori = new Apriori(transactions, parameter);

        apriori.run();

        System.out.println(apriori.getAllFreqItemsets());


        FPGrowth fpGrowth = new FPGrowth(transactions, parameter);

        fpGrowth.run();


        System.out.println(fpGrowth.getRules());


        /**
         * Wynik zgadza sie z tym otrzymanym przez skrypt z R
         *
         download.file("https://archive.ics.uci.edu/ml/machine-learning-databases/mushroom/agaricus-lepiota.data", "mush.data");

         mushSet = read.table("mush.data", header = F, sep=",", na.strings= "*")
         colnames(mushSet) <- c(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23)
         mushTR <- as(mushSet, "transactions")

         aParam  = new("APparameter", "confidence" = 0.98, "support" =0.9, "target"="rules")
         aRules <-apriori(mushTR,aParam)
         inspect(aRules)
         *
         * *******oczywiscie implementacja w R uwzglednia tylko jednoelementowe nastepniki, zatem
         * te znalezione reguly przez podany skrypt naleza do zbioru regul znalezionych przez FPGrowth
         *
         *
         */

        System.out.println("DONE");
    }
}
