package com.med.algorithm.fpgrowth;

import com.google.common.collect.Lists;
import com.med.Parameter;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class AssociationRulesGeneratorTest {
    //Test generateResultSet
    @Test
    public void shouldGenerateResultSetBeEmptyWhenSupportIsToLow() {
        List<List<String>> transactions = new ArrayList<>();

        AssociationRulesGenerator associationRulesGenerator = new AssociationRulesGenerator(transactions, new Parameter(10, 0, 1));

        List<String> set = Arrays.asList("1", "2");

        List<Itemset> freqItems = new ArrayList<>();
        freqItems.add(new Itemset(set, 0.0));

        assertEquals(
                new ArrayList<>(),
                associationRulesGenerator.findRules(freqItems)
        );
    }


    @Test
    public void shouldGenerateResultSetByFilteringOutPairsWithTooLowConfidenceWhenConfidenceIsChecked() {
        List<List<String>> transactions = Lists.newArrayList(
                Lists.newArrayList("1", "2"),
                Lists.newArrayList("1", "2"),
                Lists.newArrayList("1", "2", "3"),
                Lists.newArrayList("1", "2", "3"),
                Lists.newArrayList("3")
        );

        double minConfidence = 0.6;

        AssociationRulesGenerator associationRulesGenerator = new AssociationRulesGenerator(transactions, new Parameter(0, minConfidence, 0));


        List<Itemset> itemsets = new ArrayList<>();
        itemsets.add(new Itemset(Lists.newArrayList("1", "2", "3"), 4));

//        assertEquals(
//                Lists.newArrayList(
//                        Pair.of(Lists.newArrayList("3"), Lists.newArrayList("1", "2")),
//                        Pair.of(Lists.newArrayList("1", "3"), Lists.newArrayList("2")),
//                        Pair.of(Lists.newArrayList("2", "3"), Lists.newArrayList("1"))
////                        Pair.of(Lists.newArrayList("1", "2", "3"), Lists.newArrayList())
//                ),
//                associationRulesGenerator.findRules(itemsets, 4)
//        );
    }
}
