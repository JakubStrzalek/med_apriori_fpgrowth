package com.med.algorithm.fpgrowth.tree.header;

import com.med.algorithm.fpgrowth.tree.FPTree;
import com.med.algorithm.fpgrowth.tree.FPTreeNode;
import org.junit.Test;

import java.util.*;

import static junit.framework.TestCase.assertEquals;

public class FPHeaderTest {

    @Test
    public void shouldCreateValidFPHeader() {
        List<List<String>> transactions = Arrays.asList(
                new ArrayList<>(Arrays.asList("2a", "1a", "3a")),
                new ArrayList<>(Arrays.asList("2a", "1a")),
                new ArrayList<>(Arrays.asList("2a", "3a"))
        );

        Map<String, Integer> freqMap = new HashMap<>();
        freqMap.put("1a", 2);
        freqMap.put("2a", 3);
        freqMap.put("3a", 2);

        FPTree fpTree = new FPTree(transactions, freqMap, null);

        FPTreeNode node_2a = fpTree.getRoot().getSon("2a").get();

        FPTreeNode node_2a_1a = node_2a.getSon("1a").get();

        FPTreeNode node_2a_1a_3a = node_2a_1a.getSon("3a").get();

        FPTreeNode node_2a_3a = node_2a.getSon("3a").get();

        FPHeader header = fpTree.getFpHeader();

        Map<String, FPHeaderElement> map = header.getElements();

        assertEquals(
                Arrays.asList(
                        node_2a_1a
                )
                , map.get("1a").getNodes());

        assertEquals(
                Arrays.asList(
                        node_2a
                )
                , map.get("2a").getNodes());

        assertEquals(
                Arrays.asList(
                        node_2a_1a_3a,
                        node_2a_3a
                )
                , map.get("3a").getNodes());
    }

}
