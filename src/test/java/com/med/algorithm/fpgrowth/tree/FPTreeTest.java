package com.med.algorithm.fpgrowth.tree;

import com.med.Parameter;
import org.junit.Test;

import java.util.*;

import static junit.framework.Assert.*;
import static junit.framework.TestCase.assertEquals;

public class FPTreeTest {

    @Test
    public void shouldCreateValidFPTree() {
        List<List<String>> transactions = Arrays.asList(
                new ArrayList<>(Arrays.asList("2a", "1a", "3a")),
                new ArrayList<>(Arrays.asList("2a", "1a")),
                new ArrayList<>(Arrays.asList("2a", "3a"))
        );

        Map<String, Integer> freqMap = new HashMap<>();
        freqMap.put("1a", 2);
        freqMap.put("2a", 3);
        freqMap.put("3a", 2);

        FPTree fpTree = new FPTree(transactions, freqMap, null);

        FPTreeNode node_2a = fpTree.getRoot().getSon("2a").get();
        assertEquals(3, node_2a.getCounter());

        FPTreeNode node_2a_1a = node_2a.getSon("1a").get();
        assertEquals(2, node_2a_1a.getCounter());

        FPTreeNode node_2a_1a_3a = node_2a_1a.getSon("3a").get();
        assertEquals(1, node_2a_1a_3a.getCounter());
    }

    //TEST isEmpty()
    @Test
    public void shouldTreeBeEmptyWhenOnlyRoot() {
        FPTree fpTree = new FPTree(new ArrayList<>(), new HashMap<>(), new Parameter(0, 0, 0));

        assertTrue(fpTree.isEmpty());
    }

    @Test
    public void shouldTreeBeNotEmptyWhenOnlyOnePath() {
        List<List<String>> paths = Arrays.asList(
                new ArrayList<>(Arrays.asList("2a", "3a", "1a"))
        );

        Map<String, Integer> freqMap = new HashMap<>();
        freqMap.put("1a", 2);
        freqMap.put("2a", 2);
        freqMap.put("3a", 2);

        FPTree fpTree = new FPTree(paths, freqMap, new Parameter(0, 0, 0));

        assertFalse(fpTree.isEmpty());
    }

    @Test
    public void shouldTreeBeNotEmptyWhenTwoPaths() {
        List<List<String>> paths = Arrays.asList(
                new ArrayList<>(Arrays.asList("2a", "3a", "1a")),
                new ArrayList<>(Arrays.asList("2a", "3a", "4a"))
        );

        Map<String, Integer> freqMap = new HashMap<>();
        freqMap.put("1a", 1);
        freqMap.put("2a", 2);
        freqMap.put("3a", 2);
        freqMap.put("4a", 1);

        FPTree fpTree = new FPTree(paths, freqMap, new Parameter(0, 0, 0));

        assertFalse(fpTree.isEmpty());
    }

    //TEST containsOnlyOnePath
    @Test
    public void shouldContainsOnlyOnePathWhenOnlyOnePath() {
        List<List<String>> paths = Arrays.asList(
                new ArrayList<>(Arrays.asList("2a", "3a", "1a"))
        );

        Map<String, Integer> freqMap = new HashMap<>();
        freqMap.put("1a", 2);
        freqMap.put("2a", 2);
        freqMap.put("3a", 2);

        FPTree fpTree = new FPTree(paths, freqMap, new Parameter(0, 0, 0));

        assertTrue(fpTree.containsOnlyOnePath());
    }

    @Test
    public void shouldNotContainsOnlyOnePathWhenNoPath() {
        FPTree fpTree = new FPTree(new ArrayList<>(), new HashMap<>(), new Parameter(0, 0, 0));

        assertFalse(fpTree.containsOnlyOnePath());
    }

    @Test
    public void shouldNotContainsOnlyOnePathWhenTwoPaths() {
        List<List<String>> paths = Arrays.asList(
                new ArrayList<>(Arrays.asList("2a", "3a", "1a")),
                new ArrayList<>(Arrays.asList("2a", "3a", "4a"))
        );

        Map<String, Integer> freqMap = new HashMap<>();
        freqMap.put("1a", 1);
        freqMap.put("2a", 2);
        freqMap.put("3a", 2);
        freqMap.put("4a", 1);

        FPTree fpTree = new FPTree(paths, freqMap, new Parameter(0, 0, 0));

        assertFalse(fpTree.containsOnlyOnePath());
    }

    //TEST takeOnePath
    @Test
    public void shouldTakeOnePathWhenOnlyOnePath() {
        List<List<String>> paths = Arrays.asList(
                new ArrayList<>(Arrays.asList("2a", "3a", "1a"))
        );

        Map<String, Integer> freqMap = new HashMap<>();
        freqMap.put("1a", 2);
        freqMap.put("2a", 2);
        freqMap.put("3a", 2);

        FPTree fpTree = new FPTree(paths, freqMap, new Parameter(0, 0, 0));

        assertEquals(new HashSet<String>(
                        Arrays.asList(
                                "2a", "3a", "1a"
                        )
                ),
                fpTree.takeOnePath());
    }

    @Test(expected = IllegalStateException.class)
    public void shouldThrowIllegalStateExceptionWhenNoPath() {
        FPTree fpTree = new FPTree(new ArrayList<>(), new HashMap<>(), new Parameter(0, 0, 0));

        fpTree.takeOnePath();
    }

    @Test(expected = IllegalStateException.class)
    public void shouldThrowIllegalStateExceptionWhenTwoPaths() {
        List<List<String>> paths = Arrays.asList(
                new ArrayList<>(Arrays.asList("2a", "3a", "1a")),
                new ArrayList<>(Arrays.asList("2a", "3a", "4a"))
        );

        Map<String, Integer> freqMap = new HashMap<>();
        freqMap.put("1a", 1);
        freqMap.put("2a", 2);
        freqMap.put("3a", 2);
        freqMap.put("4a", 1);

        FPTree fpTree = new FPTree(paths, freqMap, new Parameter(0, 0, 0));

        fpTree.takeOnePath();
    }

    //TEST createConditionalBase
    @Test(expected = IllegalArgumentException.class)
    public void shouldCreateConditionalBaseThrowIllegalArgumentExcpetionWhenFPTreeWithoutArgument() {
        FPTree fpTree = new FPTree(new ArrayList<>(), new HashMap<>(), new Parameter(0, 0, 0));

        fpTree.createConditionalBase("2a");
    }

    @Test
    public void shouldCreateConditionalBaseReturnReturnPrefixPathsWhenOnlyOnePath() {
        List<List<String>> paths = Arrays.asList(
                new ArrayList<>(Arrays.asList("2a", "3a", "1a"))
        );

        Map<String, Integer> freqMap = new HashMap<>();
        freqMap.put("1a", 2);
        freqMap.put("2a", 2);
        freqMap.put("3a", 2);

        FPTree fpTree = new FPTree(paths, freqMap, new Parameter(0, 0, 0));

        List<List<String>> prefixPaths = fpTree.createConditionalBase("1a");

        assertEquals(
                Arrays.asList(
                        new ArrayList<>(Arrays.asList("2a", "3a"))
                )
                ,
                prefixPaths
        );
    }

    @Test
    public void shouldCreateConditionalBaseReturnReturnPrefixPathsWhenThreePathsAndOneNotSuited() {
        List<List<String>> paths = Arrays.asList(
                new ArrayList<>(Arrays.asList("2a", "3a", "1a")),
                new ArrayList<>(Arrays.asList("4a", "1a")),
                new ArrayList<>(Arrays.asList("4a", "3a")),
                new ArrayList<>(Arrays.asList("1a"))
        );

        Map<String, Integer> freqMap = new HashMap<>();
        freqMap.put("1a", 4);
        freqMap.put("2a", 2);
        freqMap.put("3a", 3);
        freqMap.put("4a", 2);

        FPTree fpTree = new FPTree(paths, freqMap, new Parameter(0, 0, 0));

        List<List<String>> prefixPaths = fpTree.createConditionalBase("1a");

        assertEquals(
                Arrays.asList(
                        new ArrayList<>(Arrays.asList("2a", "3a")),
                        new ArrayList<>(Arrays.asList("4a")),
                        new ArrayList<>()
                )
                ,
                prefixPaths
        );
    }

    //TEST createConditionalTree
    @Test
    public void shouldCreateEmptyConditionalTreeWhenFPTreeIsEmpty() {
        FPTree fpTree = new FPTree(new ArrayList<>(), new HashMap<>(), new Parameter(0, 0, 0));

        FPTree conditionalFpTree = fpTree.createConditionalTree(new ArrayList<>());

        assertEquals(
                0,
                conditionalFpTree.getRoot().getSons().size()
        );
    }

    @Test
    public void shouldCreateValidConditionalTreeWithAllPrefixesPaths() {
        FPTree fpTree = new FPTree(new ArrayList<>(), new HashMap<>(), new Parameter(0, 0, 0));

        FPTree conditionalFpTree = fpTree.createConditionalTree(
                Arrays.asList(
                        new ArrayList<>(Arrays.asList("2a", "3a")),
                        new ArrayList<>(Arrays.asList("4a")),
                        new ArrayList<>(Arrays.asList("2a", "5a"))
                )
        );

        assertEquals(
                1,
                conditionalFpTree.getFpHeader().getElements().get("2a").getNodes().size()
        );

        assertEquals(
                conditionalFpTree.getRoot(),
                conditionalFpTree.getFpHeader().getElements().get("4a").getNodes().get(0).getFather()
        );

    }

    @Test
    public void shouldCreateValidConditionalTreeWithBetterThanMinSupport() {
        FPTree fpTree = new FPTree(new ArrayList<>(), new HashMap<>(), new Parameter(0.3, 0, 4));

        FPTree conditionalFpTree = fpTree.createConditionalTree(
                Arrays.asList(
                        new ArrayList<>(Arrays.asList("2a", "5a", "3a")),
                        new ArrayList<>(Arrays.asList("4a")),
                        new ArrayList<>(Arrays.asList("2a", "5a")),
                        new ArrayList<>(Arrays.asList("2a"))
                )
        );

        assertNull(conditionalFpTree.getFpHeader().getElements().get("4a"));
        assertNull(conditionalFpTree.getFpHeader().getElements().get("3a"));

        assertEquals(
                3
                ,
                conditionalFpTree.getFpHeader().getElements().get("2a").getFrequency()
        );

        assertEquals(
                2
                ,
                conditionalFpTree.getFpHeader().getElements().get("5a").getFrequency()
        );


    }

}
