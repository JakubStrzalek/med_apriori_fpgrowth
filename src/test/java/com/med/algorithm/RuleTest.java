package com.med.algorithm;

import com.google.common.collect.Sets;
import org.junit.Test;

import java.util.Set;

public class RuleTest {
    @Test
    public void test() {
        Set<String> a = Sets.newHashSet("1", "2");
        Set<String> b = Sets.newHashSet("1", "2");
        Rule rule = new Rule(a, b);
        System.out.println(rule);
    }

    @Test
    public void test2() {
        Set<String> a = Sets.newHashSet("1", "2");
        Set<String> b = Sets.newHashSet();
        Rule rule = new Rule(a, b);
        System.out.println(rule);
    }
}
