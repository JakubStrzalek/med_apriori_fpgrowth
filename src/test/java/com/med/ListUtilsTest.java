package com.med;

import com.med.util.ListUtils;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class ListUtilsTest {

    @Test
    public void shouldReturnValidPowerSetWhenInputSetHaveThreeElements() {
        List<String> elements = Arrays.asList("1", "2", "3");

        assertEquals(
                validSet(),
                ListUtils.powerList(elements)
        );
    }

    private List<List<String>> validSet() {
        List<List<String>> validSet = new ArrayList<>();
        List<String> firstValidSet = new ArrayList<>();
        List<String> secondValidSet = new ArrayList<>();
        secondValidSet.add("1");
        List<String> thirdValidSet = new ArrayList<>();
        thirdValidSet.add("2");
        List<String> forthValidSet = new ArrayList<>();
        forthValidSet.add("3");
        List<String> fifthValidSet = new ArrayList<>();
        fifthValidSet.add("1");
        fifthValidSet.add("2");
        List<String> sixthValidSet = new ArrayList<>();
        sixthValidSet.add("1");
        sixthValidSet.add("3");
        List<String> seventhValidSet = new ArrayList<>();
        seventhValidSet.add("2");
        seventhValidSet.add("3");
        List<String> eightValidSet = new ArrayList<>();
        eightValidSet.add("1");
        eightValidSet.add("2");
        eightValidSet.add("3");

        validSet.add(firstValidSet);
        validSet.add(secondValidSet);
        validSet.add(thirdValidSet);
        validSet.add(forthValidSet);
        validSet.add(fifthValidSet);
        validSet.add(sixthValidSet);
        validSet.add(seventhValidSet);
        validSet.add(eightValidSet);

        return validSet;
    }

    @Test
    public void shouldReturnValidComplementarySetWhenInputSetHaveThreeElementsAndSubsetTwoElements() {
        List<String> elements = Arrays.asList("1", "2", "3");
        List<String> subset = Arrays.asList("1", "3");

        List<String> complementarySet = new ArrayList<>();
        complementarySet.add("2");

        assertEquals(
                complementarySet,
                ListUtils.complementaryList(elements, subset)
        );
    }

    @Test
    public void shouldReturnValidComplementarySetWhenInputSetHaveThreeElementsAndSubsetIsSame() {
        List<String> elements = Arrays.asList("1", "2", "3");

        List<String> complementarySet = new ArrayList<>();

        assertEquals(
                complementarySet,
                ListUtils.complementaryList(elements, elements)
        );
    }

    @Test
    public void shouldReturnValidComplementarySetWhenInputSetHaveThreeElementsAndSubsetHasNoElements() {
        List<String> elements = Arrays.asList("1", "2", "3");

        assertEquals(
                elements,
                ListUtils.complementaryList(elements, new ArrayList<>())
        );
    }
}
