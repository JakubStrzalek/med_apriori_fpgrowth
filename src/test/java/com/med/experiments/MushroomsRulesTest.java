package com.med.experiments;

import com.med.Application;
import com.med.DataLoader;
import com.med.Parameter;
import com.med.algorithm.Algorithm;
import com.med.algorithm.apriori.Apriori;
import com.med.algorithm.fpgrowth.FPGrowth;
import com.med.util.Statistics;
import com.med.util.Timer;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

public class MushroomsRulesTest {

    private static final int PROBS = 10;

    private DataLoader dataLoader = new DataLoader();
    private Timer timer = new Timer();

    @Before
    public void init() {
        Application.debug = true;
    }

    @Test // 0.2216 20.91 MB
    public void test1() throws IOException {
        runAlgorithm("Apriori", "mushrooms.data", 0.9, 1.0);
    }

    @Test // 0.0933 44.9 MB
    public void test2() throws IOException {
        runAlgorithm("FP-Growth", "mushrooms.data", 0.9, 1.0);
    }

    @Test // 0.245 15.56 MB
    public void test3() throws IOException {
        runAlgorithm("Apriori", "mushrooms.data", 0.7, 1.0);
    }

    @Test // 0.2907 33.96 MB
    public void test4() throws IOException {
        runAlgorithm("FP-Growth", "mushrooms.data", 0.7, 1.0);
    }

    @Test // 0.6771 29.31 MB
    public void test5() throws IOException {
        runAlgorithm("Apriori", "mushrooms.data", 0.5, 1.0);
    }

    @Test // 0.9706 31.86 MB
    public void test6() throws IOException {
        runAlgorithm("FP-Growth", "mushrooms.data", 0.5, 1.0);
    }

    @Test // 6.678 42.35 MB
    public void test7() throws IOException {
        runAlgorithm("Apriori", "mushrooms.data", 0.3, 1.0);
    }

    @Test // 34.3328 266.2 MB
    public void test8() throws IOException {
        runAlgorithm("FP-Growth", "mushrooms.data", 0.3, 1.0);
    }

    @Test // 0.8122
    public void test21() throws IOException {
        runAlgorithm("Apriori", "mushrooms.data", 0.5, 0.8);
    }

    @Test // 1.3989
    public void test22() throws IOException {
        runAlgorithm("FP-Growth", "mushrooms.data", 0.5, 0.8);
    }

    @Test // 0.8732
    public void test23() throws IOException {
        runAlgorithm("Apriori", "mushrooms.data", 0.5, 0.6);
    }

    @Test // 1.664
    public void test24() throws IOException {
        runAlgorithm("FP-Growth", "mushrooms.data", 0.5, 0.6);
    }

    @Test // 0.9421
    public void test25() throws IOException {
        runAlgorithm("Apriori", "mushrooms.data", 0.5, 0.3);
    }

    @Test // 1.7338
    public void test26() throws IOException {
        runAlgorithm("FP-Growth", "mushrooms.data", 0.5, 0.3);
    }

    @Test // 0.83739
    public void test27() throws IOException {
        runAlgorithm("Apriori", "mushrooms.data", 0.5, 0.1);
    }

    @Test // 1.7301
    public void test28() throws IOException {
        runAlgorithm("FP-Growth", "mushrooms.data", 0.5, 0.1);
    }

    @Test // 1.7684
    public void test31() throws IOException {
        runAlgorithm("Apriori", "chess.data", 0.9, 0.8);
    }

    @Test // 8.200899999999999
    public void test32() throws IOException {
        runAlgorithm("FP-Growth", "chess.data", 0.9, 0.8);
    }

    @Test // 0.5762
    public void test41() throws IOException {
        runAlgorithm("Apriori", "mushrooms2.data", 0.9, 0.8);
    }

    @Test // 0.4181
    public void test42() throws IOException {
        runAlgorithm("FP-Growth", "mushrooms2.data", 0.9, 0.8);
    }

    @Test // 7.052899999999999
    public void test43() throws IOException {
        runAlgorithm("Apriori", "mushrooms2.data", 0.5, 0.8);
    }

    @Test // 15.6995
    public void test46() throws IOException {
        runAlgorithm("FP-Growth", "mushrooms2.data", 0.5, 0.8);
    }




    private void runAlgorithm(String name, String data, double support, double confidence) throws IOException {
        List<List<String>> transactions = dataLoader.loadDataWithAddedArgumentLabel(data);
        Statistics.print(transactions);
        Parameter param = new Parameter(support, confidence,transactions.size());
        Algorithm alg;

        switch (name) {
            case "Apriori":
                alg  = new Apriori(transactions, param);
                break;
            case "FP-Growth":
                alg  = new FPGrowth(transactions, param);
                break;
            default:
                throw new RuntimeException("name: " + name);
        }

        double avgTime = 0.0;

        for (int i=0; i < PROBS; ++i) {
            timer.start();

            alg.run();

            timer.stop();
            timer.log(name + ". ");
            avgTime += timer.getDiffTime();
        }

        System.out.println("Average time: " + avgTime / PROBS / 1_000.0);
    }
}
