package com.med.experiments;

import com.med.Application;
import com.med.DataLoader;
import com.med.Parameter;
import com.med.algorithm.Algorithm;
import com.med.algorithm.apriori.Apriori;
import com.med.algorithm.fpgrowth.FPGrowth;
import com.med.util.Statistics;
import com.med.util.Timer;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

public class MushroomsItemsetsTest {

    private static final int PROBS = 10;

    private DataLoader dataLoader = new DataLoader();
    private Timer timer = new Timer();

    @Before
    public void init() {
        Application.debug = true;
    }

    @Test // 0.161
    public void test1() throws IOException {
        runAlgorithm("Apriori", "mushrooms.data", 0.9, 1.0);
    }

    @Test // 0.078
    public void test2() throws IOException {
        runAlgorithm("FP-Growth", "mushrooms.data", 0.9, 1.0);
    }

    @Test // 0.2134
    public void test3() throws IOException {
        runAlgorithm("Apriori", "mushrooms.data", 0.7, 1.0);
    }

    @Test // 0.1342
    public void test4() throws IOException {
        runAlgorithm("FP-Growth", "mushrooms.data", 0.7, 1.0);
    }

    @Test // 0.3521
    public void test5() throws IOException {
        runAlgorithm("Apriori", "mushrooms.data", 0.5, 1.0);
    }

    @Test // 0.3218
    public void test6() throws IOException {
        runAlgorithm("FP-Growth", "mushrooms.data", 0.5, 1.0);
    }

    @Test // 2.2751
    public void test7() throws IOException {
        runAlgorithm("Apriori", "mushrooms.data", 0.3, 1.0);
    }

    @Test // 1.224
    public void test8() throws IOException {
        runAlgorithm("FP-Growth", "mushrooms.data", 0.3, 1.0);
    }

    @Test // 32.757 328.8 MB
    public void test9() throws IOException {
        runAlgorithm("Apriori", "mushrooms.data", 0.2, 1.0);
    }

    @Test // 4.483 576.8 MB
    public void test10() throws IOException {
        runAlgorithm("FP-Growth", "mushrooms.data", 0.2, 1.0);
    }

    private void runAlgorithm(String name, String data, double support, double confidence) throws IOException {
        List<List<String>> transactions = dataLoader.loadDataWithAddedArgumentLabel(data);
        Statistics.print(transactions);
        Parameter param = new Parameter(support, confidence,transactions.size());
        Algorithm alg;

        switch (name) {
            case "Apriori":
                alg  = new Apriori(transactions, param);
                break;
            case "FP-Growth":
                alg  = new FPGrowth(transactions, param);
                break;
            default:
                throw new RuntimeException("name: " + name);
        }

        double avgTime = 0.0;

        for (int i=0; i < PROBS; ++i) {
            timer.start();

            alg.calcFreqItemsets();

            timer.stop();
            timer.log(name + ". ");
            avgTime += timer.getDiffTime();
        }

        System.out.println("Average time: " + avgTime / PROBS / 1_000.0);
    }
}
